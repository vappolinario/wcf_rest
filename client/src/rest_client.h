#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <sstream>
#include <iostream>

class RestClient
{
	public:
		std::string DownloadData(std::string url);
	private:
		static int OnReceiveData(char *data, size_t size, size_t nmemb, std::string *buffer_in);
};
