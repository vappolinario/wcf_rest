#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <sstream>
#include <iostream>

#include "rest_client.h"

using namespace rapidjson;

int main (int /*argc*/, char** /*argv*/)
{
	RestClient client;
	auto data = client.DownloadData("http://localhost:8090/library/books/sections/");
	std::cout << data << std::endl;
}


