#include "rest_client.h"
#include <curl/curl.h>

std::string RestClient::DownloadData(std::string url)
{
	// inicializa a libcurl
	CURL *curl = curl_easy_init();

	if (curl)
	{
		// definição do header da chamada
		struct curl_slist *headers = NULL;
		curl_slist_append(headers, "Accept: application/json");
		curl_slist_append(headers, "Content-Type: application/json");
		curl_slist_append(headers, "charsets: utf-8");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		// define o caminho a ser solicitado
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		// função que irá tratar os dados de retorno
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, this->OnReceiveData);
		// objeto que será passada como parâmetro para a funcão definida anteriormente
		std::string buffer;
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
		// faz o get de maneira blocante
		if (curl_easy_perform(curl) == CURLE_OK)
		{
			char *info;
			// extrai os dados que foram preenchidos no 'perform'
			auto result = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &info);
			if((result == CURLE_OK) && info)
			{
				return buffer;
			}
		}
	}

	return "";
}

int RestClient::OnReceiveData(char *data, size_t size, size_t nmemb, std::string *buffer_in)
{
    if (buffer_in != NULL)
    {
        buffer_in->append(data, size * nmemb);
        return size * nmemb;
    }
    return 0;
}
