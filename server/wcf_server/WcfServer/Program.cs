﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WcfServer
{
	class Program
	{
		static void Main(string[] args)
		{
			Uri httpUrl = new Uri("http://localhost:8090/library");
			var host = new WebServiceHost(typeof(LibraryService), httpUrl);
			var binding = new WebHttpBinding();
			host.AddServiceEndpoint(typeof(IBooks), binding, "books");
			host.Open();
			Console.WriteLine("press any key to quit");
			Console.ReadLine();
			host.Close();
		}
	}
}
