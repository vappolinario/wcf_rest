﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WcfServer
{
	[ServiceContract]
	public interface IBooks
	{
		[OperationContract]
		List<string> GetSections();
		[OperationContract]
		List<string> GetBooks(string section);
	}
}
