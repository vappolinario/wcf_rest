﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WcfServer
{
    public class LibraryService : IBooks
    {
			[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "sections/")]
			public List<string> GetSections()
			{
				return new List<string>() { "ScienceFiction", "Fantasy" };
			}

			[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "items/{section}")]
			public List<string> GetBooks(string section)
			{
				if (section.Equals("ScienceFiction", StringComparison.CurrentCultureIgnoreCase))
				{
					return new List<string>() { "I Robot", "Old Man's War" };
				}
				else if (section.Equals("Fantasy", StringComparison.CurrentCultureIgnoreCase))
				{
					return new List<string>() { "The Hobbit", "A Game of Thrones" };
				}
				return new List<string>();
			}
		}
}
